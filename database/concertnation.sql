-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2016 at 06:52 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `concertnation`
--

-- --------------------------------------------------------

--
-- Table structure for table `bands`
--

CREATE TABLE IF NOT EXISTS `bands` (
  `Band_ID` int(11) NOT NULL,
  `Band_Name` varchar(30) NOT NULL,
  `Band_Pic1` varchar(100) NOT NULL,
  `Band_Pic2` varchar(100) NOT NULL,
  `Band_Pic3` varchar(100) NOT NULL,
  `Band_Official_Page` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bands`
--

INSERT INTO `bands` (`Band_ID`, `Band_Name`, `Band_Pic1`, `Band_Pic2`, `Band_Pic3`, `Band_Official_Page`) VALUES
(1, 'Avial', 'band%20images\\avial%20logo.jpg', 'band%20images\\avial%20band.jpg', 'band%20images\\avial%20live.jpg', 'https://www.facebook.com/avialindia'),
(2, 'Escher''s Knot', 'band%20images\\eschers%20knot%20logo.jpg', 'band%20images\\eschers%20knot%20band.jpg', 'band%20images\\eschers%20knot%20live.jpg', 'https://www.facebook.com/eschersknot'),
(3, 'Indian Ocean', 'band%20images\\indian%20ocean%20logo.png', 'band%20images\\indian%20ocean%20band.jpg', 'band%20images\\indian%20ocean%20live.jpg', 'https://www.facebook.com/indianoceanmusic'),
(4, 'Fossils', 'band%20images\\fossils%20logo.jpg', 'band%20images\\fossils%20band.jpg', 'band%20images\\fossils%20live.jpg', 'https://www.facebook.com/FossilsIndia'),
(5, 'Parikrama', 'band%20images\\parikrama%20logo.jpg', 'band%20images\\parikrama%20band.jpg', 'band%20images\\parikrama%20live.jpg', 'https://www.facebook.com/parikrama'),
(6, 'Bhayanak Maut', 'band%20images\\bhayanak%20maut%20logo.png', 'band%20images\\bhayanak%20maut%20band.jpg', 'band%20images\\bhayanak%20maut%20live.jpg', 'https://www.facebook.com/bhayanakmaut'),
(7, 'Mother Jane', 'band%20images\\mother%20jane%20logo.jpg', 'band%20images\\mother%20jane%20band.jpg', 'band%20images\\mother%20jane%20live.jpg', 'https://www.facebook.com/MotherjaneOfficial'),
(8, 'Cosmic Infusion', 'band%20images\\cosmic%20infusion%20logo.jpg', 'band%20images\\cosmic%20infusion%20band.jpg', 'band%20images\\cosmic%20infusion%20live.jpg', 'https://www.facebook.com/CosmicInfusion'),
(9, 'Indus Creed', 'band%20images\\indus%20creed%20logo.png', 'band%20images\\indus%20creed%20band.jpg', 'band%20images\\indus%20creed%20live.jpg', 'https://www.facebook.com/IndusCreedNow'),
(10, 'Pentagram', 'band%20images\\pentagram%20logo.png', 'band%20images\\pentagram%20band.jpg', 'band%20images\\pentagram%20live.jpg', 'https://www.facebook.com/pentagramindia'),
(11, 'Demonic Resurrection', 'band%20images\\demonic%20resurrection%20logo.jpg', 'band%20images\\demonic%20resurrection%20band.jpg', 'band%20images\\demonic%20resurrection%20live.jpg', 'https://www.facebook.com/DemonicResurrection'),
(12, 'Undying Inc.', 'band%20images\\undying%20inc%20logo.jpg', 'band%20images\\undying%20inc%20band.jpg', 'band%20images\\undying%20inc%20live.jpg', 'https://www.facebook.com/undyinginc'),
(13, 'Kryptos', 'band%20images\\kryptos%20logo.jpg', 'band%20images\\kryptos%20band.jpg', 'band%20images\\kryptos%20live.jpg', 'https://www.facebook.com/KryptosIndia'),
(14, 'Skyharbor', 'band%20images\\skyharbor%20logo.jpg', 'band%20images\\skyharbor%20band.jpg', 'band%20images\\skyharbor%20live.jpg', 'https://www.facebook.com/skyharbormusic'),
(15, 'Inner Sanctum', 'band%20images\\inner%20sanctum%20logo.jpg', 'band%20images\\inner%20sanctum%20band.jpg', 'band%20images\\inner%20sanctum%20live.jpg', 'https://www.facebook.com/innersanctumindia'),
(16, 'Kraken', 'band%20images\\kraken%20logo.jpg', 'band%20images\\kraken%20band.jpg', 'band%20images\\kraken%20live.jpg', 'https://www.facebook.com/krakendelhi'),
(17, 'Pangea', 'band%20images\\pangea%20logo.jpg', 'band%20images\\pangea%20band.jpg', 'band%20images\\pangea%20live.jpg', 'https://www.facebook.com/pangeaindia'),
(18, 'Zygnema', 'band%20images\\zygnema%20logo.jpg', 'band%20images\\zygnema%20band.jpg', 'band%20images\\zygnema%20live.jpg', 'https://www.facebook.com/Zygnema'),
(19, 'Devoid', 'band%20images\\devoid%20logo.jpg', 'band%20images\\devoid%20band.jpg', 'band%20images\\devoid%20live.jpg', 'https://www.facebook.com/devoidindia'),
(20, 'The Down Troddence', 'band%20images\\the%20down%20troddence%20logo.jpg', 'band%20images\\the%20down%20troddence%20band.jpg', 'band%20images\\the%20down%20troddence%20live.jpg', 'https://www.facebook.com/thedowntroddence');

-- --------------------------------------------------------

--
-- Table structure for table `concerts`
--

CREATE TABLE IF NOT EXISTS `concerts` (
  `Concert_ID` int(11) NOT NULL,
  `Concert_Name` varchar(40) NOT NULL,
  `Concert_Date` date NOT NULL,
  `Concert_Venue` varchar(100) NOT NULL,
  `Concert_Venue_State` text NOT NULL,
  `Concert_Type` int(11) NOT NULL,
  `Concert_Ticket_Price` int(11) NOT NULL,
  `Concert_Pic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `concerts`
--

INSERT INTO `concerts` (`Concert_ID`, `Concert_Name`, `Concert_Date`, `Concert_Venue`, `Concert_Venue_State`, `Concert_Type`, `Concert_Ticket_Price`, `Concert_Pic`) VALUES
(1, 'Autumn Muse Poets of the Fall', '2016-08-29', 'St.Johns Medical College Lawns, Koramangala', 'karnataka', 0, 1500, 'concert%20images\\autumn%20muse%20concert.jpg'),
(2, 'Big69', '2016-01-17', 'Richardson and Cruddas, Sir J. J. Road, Byculla', 'maharashtra', 1, 1500, 'concert%20images\\big69%20concert.jpg'),
(3, 'Control Alt Delete 8.0', '2016-06-13', 'SMAAASH Club, Mumbai', 'maharashtra', 1, 100, 'concert%20images\\control%20alt%20delete%20concert.jpg'),
(4, 'Harley Rock Riders SeasonVI', '2015-10-30', 'Richardson and Cruddas, Sir J. J. Road, Byculla', 'maharashtra', 1, 1000, 'concert%20images\\harley%20rock%20riders%20concert.jpg'),
(5, 'MTV Xtreme featuring Slash', '2015-11-07', 'Reliance Jio Garden, Mumbai', 'maharashtra', 0, 1250, 'concert%20images\\mtv%20xtreme%20slash%20concert.jpg'),
(6, 'Redbull Tourbus', '2016-10-03', 'Richardson and Cruddas, Sir J. J. Road, Byculla', 'maharashtra', 0, 500, 'concert%20images\\redbull%20tourbus%20concert.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `Ticket_ID` varchar(6) NOT NULL,
  `concertid` int(11) NOT NULL,
  `cname` varchar(200) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `User_Name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `User_ID` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `First_Name` text NOT NULL,
  `Last_Name` text NOT NULL,
  `Email_ID` varchar(30) NOT NULL,
  `State` text NOT NULL,
  `City` text NOT NULL,
  `Phone` bigint(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bands`
--
ALTER TABLE `bands`
  ADD PRIMARY KEY (`Band_ID`),
  ADD UNIQUE KEY `Band_Name` (`Band_Name`),
  ADD UNIQUE KEY `Band_Official_Page` (`Band_Official_Page`),
  ADD UNIQUE KEY `Band_Pic1` (`Band_Pic1`),
  ADD UNIQUE KEY `Band_Pic2` (`Band_Pic2`),
  ADD UNIQUE KEY `Band_Pic3` (`Band_Pic3`);

--
-- Indexes for table `concerts`
--
ALTER TABLE `concerts`
  ADD PRIMARY KEY (`Concert_ID`),
  ADD UNIQUE KEY `Concert_Name` (`Concert_Name`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`Ticket_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`User_ID`),
  ADD UNIQUE KEY `Email_ID` (`Email_ID`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bands`
--
ALTER TABLE `bands`
  MODIFY `Band_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
