<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
  header("Location: Login.php");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

?>

<html>

<head>
<title>Concert Nation | Tickets</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
input {
	border-radius: 10px;
}

select {
	border-radius: 10px;
}

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 50px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">Tickets</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
  <li><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
  <li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<p>Print this page and show the tickets at the respective venues.</p><br><br><br>

<?php

$ticket = mysql_query("SELECT * FROM tickets WHERE User_ID = ".$userRow['User_ID']);

while($ticketRes = mysql_fetch_array($ticket)) {

	$concert = mysql_query("SELECT * FROM concerts WHERE Concert_ID = ".$ticketRes['concertid']);
	$concertRow = mysql_fetch_array($concert);

	echo '<div>
      <p><b><u>CONCERT NATION</u></b></p>
			<p>Ticket ID: "'.$ticketRes['Ticket_ID'].'"</p>
			<p>Name: "'.$userRow['First_Name'].'" "'.$userRow['Last_Name'].'"</p>
			<p>Email ID: "'.$userRow['Email_ID'].'"</p>
			<p>Phone: "'.$userRow['Phone'].'"</p>
			<p>Concert: "'.$concertRow['Concert_Name'].'"</p>
			<p>Date: "'.$concertRow['Concert_Date'].'"</p>
			<p>Venue: "'.$concertRow['Concert_Venue'].'", "'.$concertRow['Concert_Venue_State'].'"</p>
			<p>Price: "'.$concertRow['Concert_Ticket_Price'].'"</p>
	</div>
	<br><br><br>';
}

?>