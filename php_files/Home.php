<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
	header("Location: Home.html");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

?>

<html>

<head>
<title>Concert Nation</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }
  
body {
  padding: 50px;
}
  </style>
</head>

<body background = "backgroundImage.jpg" data-spy = "scroll" data-target = ".navbar" data-offset = "50" style = "background-attachment: fixed">

<img src = "rockFist.png" style = "width: 150px; height: 250px; margin-top: -80px" />
<span style = "font-family: Adam Warren Pro; color: #ff004d; font-size: 150px">CONCERT NATION</span>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
	<li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000" data-pause = "false">
    
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      <li data-target="#myCarousel" data-slide-to="7"></li>
    </ol>

<div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="crowdCarousel1.jpg" width="1920" height="1080">
	<div class="carousel-caption" style = "font-family: Adam Warren Pro; color: #ff004d; font-size: 35px">
        <p><b>Concert Nation</b></p>
      </div>
      </div>

	<div class="item">
        <img src="crowdCarousel5.jpg" width="1920" height="1080">
      </div>

      <div class="item">
        <img src="crowdCarousel2.jpg" width="1920" height="1080">
	<div class="carousel-caption" style = "font-family: Adam Warren Pro; color: #ff004d; font-size: 35px">
        <p><b>A prodigious site for metal heads.</b></p>
      </div>
      </div>
    

	<div class="item">
        <img src="crowdCarousel6.jpg" width="1920" height="1080">
      </div>

      <div class="item">
        <img src="crowdCarousel3.jpg" width="1920" height="1080">
	<div class="carousel-caption" style = "font-family: Adam Warren Pro; color: #ff004d; font-size: 35px">
        <p><b>We bring together all the rock and heavy metal fans of India.</b></p>
      </div>
      </div>


	<div class="item">
        <img src="crowdCarousel7.jpg" width="1920" height="1080">
      </div>

      <div class="item">
        <img src="crowdCarousel4.jpg" width="1920" height="1080">
	<div class="carousel-caption" style = "font-family: Adam Warren Pro; color: #ff004d; font-size: 35px">
        <p><b>Get updates on tours and book passes for upcoming concerts of your favourite bands.</b></p>
      </div>
      </div>


	<div class="item">
        <img src="crowdCarousel8.jpg" width="1920" height="1080">
      </div>

    </div>
  </div>
</div>

</body>

</html>