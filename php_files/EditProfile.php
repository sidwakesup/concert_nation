<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
  header("Location: Home.php");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

if(isset($_POST['saveChanges'])) {
	$Username = mysql_real_escape_string($_POST['username']);
	$Password = md5(mysql_real_escape_string($_POST['password']));
	$First_Name = mysql_real_escape_string($_POST['firstName']);
	$Last_Name = mysql_real_escape_string($_POST['lastName']);
	$Email_ID = mysql_real_escape_string($_POST['emailAddress']);
	$State = mysql_real_escape_string($_POST['state']);
	$City = mysql_real_escape_string($_POST['city']);
	$Phone = mysql_real_escape_string($_POST['phone']);

	if(mysql_query("UPDATE users SET Username = '$Username', Password = '$Password', First_Name = '$First_Name', Last_Name = '$Last_Name', Email_ID = '$Email_ID', State = '$State', City = '$City', Phone = '$Phone' WHERE User_ID = ".$userRow['User_ID'])) {
		?>
		<script>alert('Changes made successfully!');</script>
		<?php
		header("Location: Profile.php");
	}

	else {
		?>
		<script>alert('Changes failed to save.')</script>
		<?php
		header("Location: Home.php");
	}
}

?>


<html>

<head>
<title>
	Concert Nation | Edit Profile
</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
input {
	border-radius: 10px;
}

select {
	border-radius: 10px;
}

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 30px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">EDIT PROFILE</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li class="active"><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
  <li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<form method="post">
<br> First Name <br>
<input type = "text" name = "firstName" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> Last Name <br>
<input type = "text" name = "lastName" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> Email Address <br>
<input type = "text" name = "emailAddress" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> State <br>
<select name = "state" style = "color: black; font-family: Adam Warren Pro; font-size: 30px">
<option value = "andamanAndNicobarIslands">Andaman and Nicobar Islands</option>
<option value = "andraPradesh">Andhra Pradesh</option>
<option value = "arunachalPradesh">Arunachal Pradesh</option>
<option value = "assam">Assam</option>
<option value = "bihar">Bihar</option>
<option value = "chandigarh">Chandigarh</option>
<option value = "chattisgarh">Chattisgarh</option>
<option value = "dadraAndNagarHaveli">Dadra and Nagar Haveli</option>
<option value = "damanAndDiu">Daman and Diu</option>
<option value = "delhi">Delhi</option>
<option value = "goa">Goa</option>
<option value = "gujarat">Gujarat</option>
<option value = "haryana">Haryana</option>
<option value = "himachalPradesh">Himachal Pradesh</option>
<option value = "jammuAndKashmir">Jammu and Kashmir</option>
<option value = "jharkhand">Jharkhand</option>
<option value = "karnataka">Karnataka</option>
<option value = "kerala">Kerala</option>
<option value = "lakshadweep">Lakshadweep</option>
<option value = "madhyaPradesh">Madhya Pradesh</option>
<option value = "maharashtra">Maharashtra</option>
<option value = "manipur">Manipur</option>
<option value = "meghalaya">Meghalaya</option>
<option value = "mizoram">Mizoram</option>
<option value = "nagaland">Nagaland</option>
<option value = "odisha">Odisha</option>
<option value = "puducherry">Puducherry</option>
<option value = "punjab">Punjab</option>
<option value = "rajasthan">Rajasthan</option>
<option value = "sikkim">Sikkim</option>
<option value = "tamilNadu">Tamil Nadu</option>
<option value = "telangana">Telangana</option>
<option value = "tripura">Tripura</option>
<option value = "uttarPradesh">Uttar Pradesh</option>
<option value = "uttarakhand">Uttarakhand</option>
<option value = "westBengal">West Bengal</option>
</select>
<br> City <br>
<input type = "text" name = "city" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> Phone <br>
<input type = "number" name = "phone" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> Username <br>
<input type = "text" name = "username" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br> Password <br>
<input type = "password" name = "password" style = "color: black; font-size: 30px; font-family: Adam Warren Pro">
<br><br>
<input type = "submit" name = "saveChanges" value = "Save Changes" style = "font-size: 30px; font-family: Adam Warren Pro; background: black; color: #ff004d">
</form>

</body>

</html>