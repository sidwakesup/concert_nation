<?php

session_start();

if(isset($_SESSION['user'])!="") {
	header("Location: Home.php");
}

include_once 'dbconnect.php';

if(isset($_POST['signUp'])) {
	$Username = mysql_real_escape_string($_POST['username']);
	$Password = md5(mysql_real_escape_string($_POST['password']));
	$First_Name = mysql_real_escape_string($_POST['firstName']);
	$Last_Name = mysql_real_escape_string($_POST['lastName']);
	$Email_ID = mysql_real_escape_string($_POST['emailAddress']);
	$State = mysql_real_escape_string($_POST['state']);
	$City = mysql_real_escape_string($_POST['city']);
	$Phone = mysql_real_escape_string($_POST['phone']);

	if(mysql_query("INSERT INTO users(Username, Password, First_Name, Last_Name, Email_ID, State, City, Phone) VALUES('$Username', '$Password', '$First_Name', '$Last_Name', '$Email_ID', '$State', '$City', '$Phone')")) {
		?>
		<script>alert('Successfully signed up!');</script>
		<?php
		header("Location: Login.php");
	}

	else {
		?>
		<script>alert('Sign up failed. Please enter valid and unique details.'); </script>
		<?php
	}
}

?>

<html>

<head>
<title>Concert Nation | Sign Up</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
input {
	border-radius: 10px;
}

select {
	border-radius: 10px;
}

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 30px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">Sign Up</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li class="active"><a href="SignUp.php"><span class = "glyphicon glyphicon-user"></span>Sign Up</a></li>
	<li><a href="Login.php"><span class = "glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<form name = "signupform" method="post">
<br> First Name <br>
<input type = "text" name = "firstName" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> Last Name <br>
<input type = "text" name = "lastName" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> Email Address <br>
<input type = "text" name = "emailAddress" id = "emailAddress" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> State <br>
<select name = "state" style = "color: black; font-family: Adam Warren Pro; font-size: 30px" required>
<option value = "andamanAndNicobarIslands">Andaman and Nicobar Islands</option>
<option value = "andraPradesh">Andhra Pradesh</option>
<option value = "arunachalPradesh">Arunachal Pradesh</option>
<option value = "assam">Assam</option>
<option value = "bihar">Bihar</option>
<option value = "chandigarh">Chandigarh</option>
<option value = "chattisgarh">Chattisgarh</option>
<option value = "dadraAndNagarHaveli">Dadra and Nagar Haveli</option>
<option value = "damanAndDiu">Daman and Diu</option>
<option value = "delhi">Delhi</option>
<option value = "goa">Goa</option>
<option value = "gujarat">Gujarat</option>
<option value = "haryana">Haryana</option>
<option value = "himachalPradesh">Himachal Pradesh</option>
<option value = "jammuAndKashmir">Jammu and Kashmir</option>
<option value = "jharkhand">Jharkhand</option>
<option value = "karnataka">Karnataka</option>
<option value = "kerala">Kerala</option>
<option value = "lakshadweep">Lakshadweep</option>
<option value = "madhyaPradesh">Madhya Pradesh</option>
<option value = "maharashtra">Maharashtra</option>
<option value = "manipur">Manipur</option>
<option value = "meghalaya">Meghalaya</option>
<option value = "mizoram">Mizoram</option>
<option value = "nagaland">Nagaland</option>
<option value = "odisha">Odisha</option>
<option value = "puducherry">Puducherry</option>
<option value = "punjab">Punjab</option>
<option value = "rajasthan">Rajasthan</option>
<option value = "sikkim">Sikkim</option>
<option value = "tamilNadu">Tamil Nadu</option>
<option value = "telangana">Telangana</option>
<option value = "tripura">Tripura</option>
<option value = "uttarPradesh">Uttar Pradesh</option>
<option value = "uttarakhand">Uttarakhand</option>
<option value = "westBengal">West Bengal</option>
</select>
<br> City <br>
<input type = "text" name = "city" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> Phone <br>
<input type = "number" name = "phone" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> Username <br>
<input type = "text" name = "username" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br> Password <br>
<input type = "password" name = "password" style = "color: black; font-size: 30px; font-family: Adam Warren Pro" required>
<br><br>
<input type = "checkbox" name = "notARobot" value = "notARobot" style = "font-size: 30px; font-family: Adam Warren Pro" required> I am not a robot.
<br><br>
<input type = "submit" name = "signUp" value = "Sign Up" style = "font-size: 30px; font-family: Adam Warren Pro; background: black; color: #ff004d">
</form>


<div style = "position: absolute; top: 400px; right: 100px; width: 400px; height: 300px; font-size: 50px">
<p>Already have an account? Login by clicking <a href = "Login.php">here</a>.</p>
</div>

</body>

</html>