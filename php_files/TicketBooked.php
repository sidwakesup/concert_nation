<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
  header("Location: Login.php");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

?>

<html>

<head>
<title>Concert Nation | Tickets Booked</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
input {
	border-radius: 10px;
}

select {
	border-radius: 10px;
}

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 50px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">Ticket Booked</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
  <li><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
  <li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<p><b>Your ticket has been successfully booked. Enjoy the gig!</b></p>
<br><br><br>

<a href="GenerateTickets.php">Generate tickets</a><br><br>

<p>OR</p>

<a href="Home.php">Go back Home.</a>