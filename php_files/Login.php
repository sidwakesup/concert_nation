<?php

session_start();

include_once 'dbconnect.php';

if(isset($_SESSION['user'])!="") {
	header("Location: Home.php");
}

if(isset($_POST['login'])) {
	$Username = mysql_real_escape_string($_POST['username']);
	$Password = mysql_real_escape_string($_POST['password']);

	$res = mysql_query("SELECT * FROM users WHERE Username = '$Username'");
	$row = mysql_fetch_array($res);
	if($row['Password']==md5($Password)) {
		$_SESSION['user'] = $row['User_ID'];
		header("Location: Home.php");
	}

	else {
		?>
		<script>alert('Invalid details.');</script>
		<?php
	}
}

?>

<html>

<head>
<title>Concert Nation | Login</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>
input {
	border-radius: 10px;
}

select {
	border-radius: 10px;
}

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 50px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">Login</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li><a href="SignUp.php"><span class = "glyphicon glyphicon-user"></span>Sign Up</a></li>
	<li class="active"><a href="Login.php"><span class = "glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<form method="post">
<br> Username <br>
<input type = "text" name = "username" style = "font-size: 50px; color: black; font-family: Adam Warren Pro">
<br> Password <br>
<input type = "password" name = "password" style = "font-size: 50px; color: black; font-family: Adam Warren Pro">
<br><br>
<input type = "submit" name = "login" value = "Login" style = "font-size: 50px; font-family: Adam Warren Pro; color: #ff004d; background-color: black">
</form>

<div style = "position: absolute; top: 400px; right: 100px; width: 400px; height: 300px">
<p>Don't have an account? Create one by clicking <a href = "SignUp.php">here</a>.</p>
</div>

</body>

</html>