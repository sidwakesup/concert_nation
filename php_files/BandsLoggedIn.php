<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
  header("Location: BandsLoggedOut.php");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

$numbers = range(1, 20);
shuffle($numbers);

$res1 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[0]);
$userRow1 = mysql_fetch_array($res1);

$res2 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[1]);
$userRow2 = mysql_fetch_array($res2);

$res3 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[2]);
$userRow3 = mysql_fetch_array($res3);

$res4 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[3]);
$userRow4 = mysql_fetch_array($res4);

$res5 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[4]);
$userRow5 = mysql_fetch_array($res5);

$res6 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[5]);
$userRow6 = mysql_fetch_array($res6);

$res7 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[6]);
$userRow7 = mysql_fetch_array($res7);

$res8 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[7]);
$userRow8 = mysql_fetch_array($res8);

$res9 = mysql_query("SELECT * FROM bands WHERE Band_ID=".$numbers[8]);
$userRow9 = mysql_fetch_array($res9);

?>


<html>

<head>

<title>Concert Nation | Bands</title>

<style>
td {
    border-right: 20px solid transparent;
    -webkit-background-clip: padding;
    -moz-background-clip: padding;
    background-clip: padding-box;
}

img {
  width: 400px;
  height: 400px;
}

body {
  padding: 50px;
}
</style>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>

<body background = "backgroundImage.jpg">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">Bands</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li class="active"><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
  <li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<p style = "font-family: Adam Warren Pro; font-size: 30px; color: #ff004d">Explore the world of rock and metal. Check out the Facebook pages of these bands by clicking on the images.</p>

<table align = "center">
<tr>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
       <?php echo "<a href = '".$userRow1['Band_Official_Page']."' target='_blank'><img src = '".$userRow1['Band_Pic1']."'></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow1['Band_Official_Page']."' target='_blank'><img src = '".$userRow1['Band_Pic2']."'></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow1['Band_Official_Page']."' target='_blank'><img src = '".$userRow1['Band_Pic3']."'></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow2['Band_Official_Page']."' target='_blank'><img src='".$userRow2['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow2['Band_Official_Page']."' target='_blank'><img src='".$userRow2['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow2['Band_Official_Page']."' target='_blank'><img src='".$userRow2['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>
</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow3['Band_Official_Page']."' target='_blank'><img src='".$userRow3['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow3['Band_Official_Page']."' target='_blank'><img src='".$userRow3['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow3['Band_Official_Page']."' target='_blank'><img src='".$userRow3['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
</tr>

<tr>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow4['Band_Official_Page']."' target='_blank'><img src='".$userRow4['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow4['Band_Official_Page']."' target='_blank'><img src='".$userRow4['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow4['Band_Official_Page']."' target='_blank'><img src='".$userRow4['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow5['Band_Official_Page']."' target='_blank'><img src='".$userRow5['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow5['Band_Official_Page']."' target='_blank'><img src='".$userRow5['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow5['Band_Official_Page']."' target='_blank'><img src='".$userRow5['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow6['Band_Official_Page']."' target='_blank'><img src='".$userRow6['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow6['Band_Official_Page']."' target='_blank'><img src='".$userRow6['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow6['Band_Official_Page']."' target='_blank'><img src='".$userRow6['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
</tr>
<tr>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow7['Band_Official_Page']."' target='_blank'><img src='".$userRow7['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow7['Band_Official_Page']."' target='_blank'><img src='".$userRow7['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
       <?php echo "<a href = '".$userRow7['Band_Official_Page']."' target='_blank'><img src='".$userRow7['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow8['Band_Official_Page']."' target='_blank'><img src='".$userRow8['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
       <?php echo "<a href = '".$userRow8['Band_Official_Page']."' target='_blank'><img src='".$userRow8['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow8['Band_Official_Page']."' target='_blank'><img src='".$userRow8['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
<td width = 400>

<div class="container" style="width: 400px">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" style = "width: 400px; height: 400px" data-pause = "false">

<div class="carousel-inner" role="listbox" width = 200 height = 400>
      <div class="item active" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow9['Band_Official_Page']."' target='_blank'><img src='".$userRow9['Band_Pic1']."' /></a>"; ?>
      </div>

      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow9['Band_Official_Page']."' target='_blank'><img src='".$userRow9['Band_Pic2']."' /></a>"; ?>
      </div>
    
      <div class="item" style = "width: 400px; height: 400px">
        <?php echo "<a href = '".$userRow9['Band_Official_Page']."' target='_blank'><img src='".$userRow9['Band_Pic3']."' /></a>"; ?>
      </div>

    </div>
  </div>
</div>

</td>
</tr>
</table>

</body>

</html>