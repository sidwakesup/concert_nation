<?php

session_start();

include_once 'dbconnect.php';

if(!isset($_SESSION['user'])) {
  header("Location: Home.php");
}

$res = mysql_query("SELECT * FROM users WHERE User_ID=".$_SESSION['user']);
$userRow = mysql_fetch_array($res);

if(isset($_POST['editProfile'])) {
	header("Location: EditProfile.php");
}

if(isset($_POST['deactivateAccount'])) {
	header("Location: DeactivateAccount.php");
}

if(isset($_POST['generateTickets'])) {
  header("Location: GenerateTickets.php");
}

?>


<html>

<head>

<title>Concert Nation | Profile</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style>

body {
  padding: 50px;
}
</style>

</head>

<body background = "backgroundImage.jpg" style = "background-attachment: fixed; font-size: 50px; font-family: Adam Warren Pro; color: #ff004d">
<p style = "font-size: 100px; color: #ff004d; font-family: Adam Warren Pro" align = "center">PROFILE</p>

<nav class="navbar navbar-inverse" style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro">
  <div class="container-fluid">

    <div>
      <ul class="nav navbar-nav">
        <li><a href="Home.php">Home</a></li>
        <li><a href="News.php">News</a></li>
        <li><a href="BandsLoggedIn.php">Bands</a></li>
        <li><a href="ContactUs.php">Contact Us</a></li>
	</ul>
	<ul class = "nav navbar-nav navbar-right">
	<li class="active"><a href="Profile.php"><span class = "glyphicon glyphicon-user" ><span style = "font-size: 20px; color: #ff004d; font-family: Adam Warren Pro"><?php echo $userRow['Username']; ?></span></span></a></li>
  <li><a href="Logout.php?logout"><span class = "glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<br>
<h1 style = "font-size: 60px; padding-left: 20px">PROFILE DETAILS</h1>
<p>Username: <?php echo $userRow['Username']; ?></p>
<p>Email ID: <?php echo $userRow['Email_ID']; ?></p>
<p>First Name: <?php echo $userRow['First_Name']; ?></p>
<p>Last Name: <?php echo $userRow['Last_Name']; ?></p>
<p>State: <?php echo $userRow['State']; ?></p>
<p>City: <?php echo $userRow['City']; ?></p>
<p>Phone: <?php echo $userRow['Phone']; ?></p>

<form method = "post">
<input type = "submit" id = "editProfile" name = "editProfile" value = "Edit Profile" style = "font-size: 30px; font-family: Adam Warren Pro; color: #ff004d; background-color: black; position: absolute; top: 300px; right: 20px">
<input type = "submit" id = "deactivateAccount" name = "deactivateAccount" value = "Deactivate Account" style = "font-size: 30px; font-family: Adam Warren Pro; color: #ff004d; background-color: black; position: absolute; top: 300px; right: 250px">
<input type = "submit" id = "generateTickets" name = "generateTickets" value = "Generate Tickets" style = "font-size: 30px; font-family: Adam Warren Pro; color: #ff004d; background-color: black; position: absolute; top: 300px; right: 650px">
</form>

</html>